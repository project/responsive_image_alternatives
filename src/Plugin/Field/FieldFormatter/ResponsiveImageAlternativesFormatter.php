<?php

namespace Drupal\responsive_image_alternatives\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\responsive_image\Plugin\Field\FieldFormatter\ResponsiveImageFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'responsive_image_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "responsive_image_alternatives_formatter",
 *   label = @Translation("Responsive Image Alternatives formatter"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class ResponsiveImageAlternativesFormatter extends ResponsiveImageFormatter {

  /**
   * @var EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a ResponsiveImageAlternativesFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityStorageInterface $responsive_image_style_storage
   *   The responsive image style storage.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   *   The link generator service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    EntityStorageInterface $responsive_image_style_storage,
    EntityStorageInterface $image_style_storage,
    LinkGeneratorInterface $link_generator,
    AccountInterface $current_user,
    EntityFieldManagerInterface $entity_field_manager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $responsive_image_style_storage, $image_style_storage, $link_generator, $current_user);

    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity.manager')->getStorage('responsive_image_style'),
      $container->get('entity.manager')->getStorage('image_style'),
      $container->get('link_generator'),
      $container->get('current_user'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'alternative_fields' => '',
      'alternative_responsive_image_style_field' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'alternative_fields' => [
        '#type' => 'textarea',
        '#description' => $this->t('Provide alternative image fields definitions in YAML format. Example:  <pre>
            field_image_portrait:
              - breakpoint_name.portrait
            field_image_wide_portrait:
              - breakpoint_name.wide_portrait_mobile
              - breakpoint_name.wide_portrait_tablet
        </pre>'),
        '#title' => $this->t('Specify alternative image fields'),
        '#default_value' => $this->getSetting('alternative_fields'),
        '#element_validate' => [
          [$this, 'responsiveImageAlternativesValidate']
        ]
      ],
      'alternative_responsive_image_style_field' => [
        '#type' => 'select',
        '#description' => $this->t('Provide alternative responsive image style field to override responsive image style on rendering.'),
        '#title' => $this->t('Specify responsive image style override field'),
        '#default_value' => $this->getSetting('alternative_responsive_image_style_field'),
        // Adds the empty option to the options list.
        '#empty_value' => '',
        '#options' => $this->getResonsiveImageStyleOverrideFieldOptions(),
      ]
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // @TODO: Implement summary.
    $summary = $summary + parent::settingsSummary();
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    $entity = $items->getEntity();

    $alternative_fields_settings = $this->getSetting('alternative_fields');
    $alternative_fields_settings = Yaml::decode($alternative_fields_settings);

    $alternatives = [];
    foreach ($elements as $delta => &$element) {
      foreach ($alternative_fields_settings as $field_name => $breakpoints)  {
        $field_list = $entity->{$field_name};
        if (!$field_list->isEmpty()) {
          $entities = [$field_list];
          $this->prepareView($entities);
          $alternative_files = $this->getEntitiesToView($field_list, $langcode);
          $image = $alternative_files[0]->_referringItem;
          foreach ($breakpoints as $breakpoint) {
            $alternatives[$breakpoint] = $image->entity->getFileUri();
          }
        }
      }

      $responsive_image_style_field = $this->getSetting('alternative_responsive_image_style_field');
      if (!empty($alternatives) || $responsive_image_style_field) {
        $element['#theme'] = 'responsive_image_alternatives_formatter';
        $element['#alternatives'] = $alternatives;

        if ($responsive_image_style_field && $entity->{$responsive_image_style_field} && !$entity->{$responsive_image_style_field}->isEmpty()) {
          $element['#responsive_image_style_override'] = $entity->{$responsive_image_style_field}->value;
        }
      }
    }

    return $elements;
  }

  /**
   * Element validation handler for responsive image alternatives settings.
   */
  public function responsiveImageAlternativesValidate($element, FormStateInterface $form_state) {
    if (empty($element['#value'])) {
      return;
    }

    $settings = Yaml::decode($element['#value']);
    if (!is_array($settings) || !is_array(reset($settings))) {
      $form_state->setError($element, $this->t('Please specify a valid config in YAML format.'));
    }

    // @TODO: Validate that fields and breakpoints actually exist.
  }

  /**
   * Returns an array of existing field storages that can be added to a bundle.
   *
   * @return array
   *   An array of existing field storages keyed by name.
   */
  protected function getResonsiveImageStyleOverrideFieldOptions() {
    // Load the field_storages and build the list of options.
    return array_map(
      function ($field_storage) {
        return $field_storage->getName();
      },
      array_filter(
        $this->entityFieldManager->getFieldStorageDefinitions(
          $this->fieldDefinition->get('entity_type')
        ),
        function ($field_storage, $field_name) {
          return $field_storage->getSetting('allowed_values_function') == 'responsive_image_alternatives_responsive_image_styles_list_callback';
        },
        ARRAY_FILTER_USE_BOTH
      )
    );
  }
}
