<?php

namespace Drupal\responsive_image_alternatives\Element;

use Drupal\responsive_image\Element\ResponsiveImage;

/**
 * Provides a responsive image alternatives element.
 *
 * @RenderElement("responsive_image_alternatives")
 */
class ResponsiveImageAlternatives extends ResponsiveImage {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $info = parent::getInfo();

    $info['#theme'] = 'responsive_image_alternatives';
    return $info;
  }
}
